package com.epam.theater;

import com.epam.theater.config.TheaterConfiguration;
import com.epam.theater.entity.Auditorium;
import com.epam.theater.entity.Event;
import com.epam.theater.entity.EventRating;
import com.epam.theater.service.AuditoriumService;
import com.epam.theater.service.EventService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDateTime;
import java.util.*;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TheaterConfiguration.class)
public class EventServiceTest {

    private final String AUDITORIUM_NAME = "Pokemon room";

    @Autowired
    private AuditoriumService auditoriumService;

    @Autowired
    private EventService eventService;

    private Event event;

    @Before
    public void initEvent() {
        event = new Event();
        event.setId(6L);
        event.setBasePrice(100);
        event.setName("Paulink");
        event.setRating(EventRating.HIGH);
        NavigableSet<LocalDateTime> airDates = new TreeSet<>();
        LocalDateTime today = LocalDateTime.now();
        airDates.add(today);
        event.setAirDates(airDates);
        NavigableMap<LocalDateTime, Auditorium> auditoriums = new TreeMap<>();
        auditoriums.put(today, auditoriumService.getByName(AUDITORIUM_NAME));
        event.setAuditoriums(auditoriums);
    }

    @Test
    public void testSave() {
        Event savedEvent = eventService.save(event);
        assertTrue(savedEvent.getName().equals(event.getName()));
    }


    @Test
    public void testRemove() {
        eventService.remove(event);
        assertFalse(eventService.getAll().contains(event));
    }

    @Test
    public void testGetById() {
        Event findEvent = eventService.getById(6L);
        assertTrue(findEvent.equals(event));
    }

    @Test
    public void testGetAll() {
        Collection<Event> events = eventService.getAll();
        assertTrue(events != null);
    }

    @Test
    public void testGetByName() {
        Event findEvent = eventService.getByName("Paulink");
        assertTrue(findEvent.equals(event));
    }
}
