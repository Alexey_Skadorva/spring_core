package com.epam.theater;

import com.epam.theater.config.TheaterConfiguration;
import com.epam.theater.entity.User;
import com.epam.theater.service.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Collection;

import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TheaterConfiguration.class)
public class UserServiceTest {

    @Autowired
    private UserService userService;

    private User user;

    @Before
    public void initUser() {
        user = new User("Alexey", "Skadorva", "lesh@gmail.com");
        user.setId(6L);
    }

    @Test
    public void testSave() {
        userService.save(user);
        assertTrue(userService.getAll().contains(user));
    }

    @Test
    public void testRemove() {
        userService.remove(user);
        assertTrue(!userService.getAll().contains(user));
    }

    @Test
    public void testGetById() {
        User findUser = userService.getById(6L);
        assertTrue(findUser.getId() == user.getId());
    }

    @Test
    public void testGetAll() {
        Collection<User> users = userService.getAll();
        assertTrue(users != null);
    }

    @Test
    public void testGetByEmail() {
        // userService.save(user);
        User findUser = userService.getUserByEmail("lesh@gmail.com");
        assertTrue(findUser.getEmail().equals(user.getEmail()));
    }
}
