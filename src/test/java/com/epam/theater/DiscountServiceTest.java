package com.epam.theater;

import com.epam.theater.config.TheaterConfiguration;
import com.epam.theater.entity.Auditorium;
import com.epam.theater.entity.Event;
import com.epam.theater.entity.EventRating;
import com.epam.theater.entity.User;
import com.epam.theater.service.AuditoriumService;
import com.epam.theater.service.DiscountService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDateTime;
import java.util.NavigableMap;
import java.util.NavigableSet;
import java.util.TreeMap;
import java.util.TreeSet;

import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TheaterConfiguration.class)
public class DiscountServiceTest {

    private final String AUDITORIUM_NAME = "Pokemon room";

    @Autowired
    private AuditoriumService auditoriumService;

    @Autowired
    private DiscountService discountService;

    private User user;

    private Event event;

    @Before
    public void initUser() {
        user = new User("Alexey", "Skadorva", "leshaskadorva@gmail.com");
        user.setId(1L);
        event = new Event();
        event.setId(1L);
        event.setBasePrice(100);
        event.setName("Paulinka");
        event.setRating(EventRating.HIGH);
        NavigableSet<LocalDateTime> airDates = new TreeSet<>();
        LocalDateTime today = LocalDateTime.now();
        airDates.add(today);
        event.setAirDates(airDates);
        NavigableMap<LocalDateTime, Auditorium> auditoriums = new TreeMap<>();
        auditoriums.put(today, auditoriumService.getByName(AUDITORIUM_NAME));
        event.setAuditoriums(auditoriums);
    }

    @Test
    public void testGetDiscount() {
        int discount = discountService.getDiscount(user, event, LocalDateTime.now(), 100);
        assertTrue(discount != 0);
    }
}
