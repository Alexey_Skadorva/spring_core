package com.epam.theater;

import com.epam.theater.config.TheaterConfiguration;
import com.epam.theater.entity.*;
import com.epam.theater.service.AuditoriumService;
import com.epam.theater.service.BookingService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDateTime;
import java.util.*;

import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TheaterConfiguration.class)
public class BookingServiceTest {

    private final String AUDITORIUM_NAME = "Pokemon room";

    @Autowired
    private AuditoriumService auditoriumService;

    @Autowired
    private BookingService bookingService;

    private User user;
    private Event event;
    private Set<Long> seats;
    private Set<Ticket> tickets;

    @Before
    public void initUser() {
        user = new User("Alexey", "Skadorva", "leshaskadorva@gmail.com");
        user.setId(1L);
        event = new Event();
        event.setId(1L);
        event.setBasePrice(100);
        event.setName("Paulinka");
        event.setRating(EventRating.HIGH);
        Event secondEvent = new Event();
        secondEvent.setId(2L);
        secondEvent.setBasePrice(20);
        secondEvent.setName("Paulinka");
        secondEvent.setRating(EventRating.LOW);
        NavigableSet<LocalDateTime> airDates = new TreeSet<>();
        LocalDateTime today = LocalDateTime.now();
        airDates.add(today);
        event.setAirDates(airDates);
        NavigableMap<LocalDateTime, Auditorium> auditoriums = new TreeMap<>();
        auditoriums.put(today, auditoriumService.getByName(AUDITORIUM_NAME));
        event.setAuditoriums(auditoriums);
        seats = new HashSet<>();
        seats.add(1L);
        seats.add(2L);
        tickets = new HashSet<>();
        tickets.add(new Ticket(user, event, LocalDateTime.now(), 200L));
        tickets.add(new Ticket(user, secondEvent, LocalDateTime.now(), 20L));
        tickets.add(new Ticket(user, event, LocalDateTime.now(), 23L));
    }

    @Test
    public void testGetTicketsPrice() {
        double price = bookingService.getTicketsPrice(event, LocalDateTime.now(), user, seats);
        System.out.println(price);
        assertTrue(price != 0);
    }

    @Test
    public void testGetPurchasedTicketsForEvent() {
        Set<Ticket> purchasedTickets = bookingService.getPurchasedTicketsForEvent(event, LocalDateTime.now());
        assertTrue(purchasedTickets != null);
    }

    @Test
    public void testBookTickets() {
        bookingService.bookTickets(tickets);
        Set<Ticket> purchasedTickets = bookingService.getPurchasedTicketsForEvent(event, LocalDateTime.now());
        assertTrue(purchasedTickets.size() != 3);
    }
}
