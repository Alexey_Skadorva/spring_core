package com.epam.theater;

import com.epam.theater.config.TheaterConfiguration;
import com.epam.theater.entity.Auditorium;
import com.epam.theater.service.AuditoriumService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Set;

import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TheaterConfiguration.class)
public class AuditoriumServiceTest {

    private final String AUDITORIUM_NAME = "Pokemon room";

    @Autowired
    private AuditoriumService auditoriumService;

    @Test
    public void testGetAll() {
        Set<Auditorium> auditoriumList = auditoriumService.getAll();
        assertTrue(auditoriumList.size() != 0);
    }

    @Test
    public void testGetByName() {
        Auditorium auditoriumList = auditoriumService.getByName(AUDITORIUM_NAME);
        assertTrue(auditoriumList != null);
    }
}
