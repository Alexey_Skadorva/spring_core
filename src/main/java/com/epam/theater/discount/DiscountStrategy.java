package com.epam.theater.discount;

import com.epam.theater.entity.Event;
import com.epam.theater.entity.User;

import java.time.LocalDateTime;

public interface DiscountStrategy {
    byte getDiscount(User user, Event event, LocalDateTime airDateTime, long numberOfTickets);
}
