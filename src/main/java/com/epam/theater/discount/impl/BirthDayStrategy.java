package com.epam.theater.discount.impl;

import com.epam.theater.discount.DiscountStrategy;
import com.epam.theater.entity.Event;
import com.epam.theater.entity.User;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.LocalDateTime;

@Service
public class BirthDayStrategy implements DiscountStrategy {
    private static byte DISCOUNT = 5;

    @Override
    public byte getDiscount(User user, Event event, LocalDateTime airDateTime, long numberOfTickets) {
        if (user.getBirthDay() != null && Duration.between(user.getBirthDay(), airDateTime.toLocalDate()).abs().toDays() < 5) {
            return DISCOUNT;
        }
        return 0;
    }
}
