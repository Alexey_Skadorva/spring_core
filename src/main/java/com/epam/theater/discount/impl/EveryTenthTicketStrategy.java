package com.epam.theater.discount.impl;

import com.epam.theater.discount.DiscountStrategy;
import com.epam.theater.entity.Event;
import com.epam.theater.entity.User;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class EveryTenthTicketStrategy implements DiscountStrategy {

    @Override
    public byte getDiscount(User user, Event event, LocalDateTime airDateTime, long numberOfTickets) {
        if (user != null) {
             /*
             Discount if user registered
             */
            long currentNumberOfTickets = user.getTickets().size() + numberOfTickets;
            if ((currentNumberOfTickets / 10 - user.getTickets().size() / 10) > 0) {
                return (byte) (numberOfTickets - (numberOfTickets - (currentNumberOfTickets / 10 - user.getTickets().size() / 10) * 0.5));
            }
        } else {
            /*
             Discount if user not registered
             */
            if (numberOfTickets / 10 > 0) {
                return (byte) (numberOfTickets - (numberOfTickets - numberOfTickets / 10 * 0.5));
            }
        }
        return 0;
    }
}
