package com.epam.theater.config;

import com.epam.theater.entity.Auditorium;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Configuration
@ComponentScan("com.epam.theater")
@EnableAspectJAutoProxy(proxyTargetClass = true)
@PropertySource({"classpath:auditorium.properties", "classpath:jdbc.properties"})
public class TheaterConfiguration {

    @Value("${jdbc.driverClassName}")
    String driver;
    @Value("${jdbc.url}")
    String url;
    @Value("${jdbc.username}")
    String userName;
    @Value("${jdbc.password}")
    String password;
    @Autowired
    private Auditorium auditorium;

    @Bean(name = "dataSource")
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(driver);
        dataSource.setPassword(password);
        dataSource.setUrl(url);
        dataSource.setUsername(userName);
        return dataSource;
    }

    @Bean(name = "jdbcTemplate")
    public JdbcTemplate jdbcTemplate() {
        return new JdbcTemplate(dataSource());
    }

    @Bean
    public Set<Auditorium> getAuditoriums() {
        return Stream.of(auditorium).collect(Collectors.toSet());
    }
}