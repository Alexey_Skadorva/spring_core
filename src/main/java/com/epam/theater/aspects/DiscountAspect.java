package com.epam.theater.aspects;

import com.epam.theater.entity.User;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.aop.aspectj.annotation.AnnotationAwareAspectJAutoProxyCreator;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@Aspect
@Component
public class DiscountAspect {
    final static Logger logger = Logger.getLogger(String.valueOf(DiscountAspect.class));
    List<User> luckyUsers = new ArrayList<>();
    List<Byte> discounts = new ArrayList<>();

    @Bean
    public AnnotationAwareAspectJAutoProxyCreator annotationAwareAspectJAutoProxyCreator() {
        return new AnnotationAwareAspectJAutoProxyCreator();
    }

    @Before("execution(* com.epam.theater.service.DiscountService.getDiscount(..)) && args(user,..)")
    private void discountUser(User user) {
        logger.info("Get discount for " + user);
        luckyUsers.add(user);
    }

    @AfterReturning(pointcut = "execution(* com.epam.theater.discount.DiscountStrategy.getDiscount(..))", returning = "discount")
    private void getDiscount(byte discount) {
        if (discount > 0) {
            logger.info("Discount " + discount + "%");
            discounts.add(discount);
        }
    }
}
