package com.epam.theater.aspects;

import com.epam.theater.entity.Ticket;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.aop.aspectj.annotation.AnnotationAwareAspectJAutoProxyCreator;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.Random;
import java.util.Set;
import java.util.logging.Logger;

@Aspect
@Component
public class LuckyWinnerAspect {
    private final Random random = new Random();
    final static Logger logger = Logger.getLogger(String.valueOf(LuckyWinnerAspect.class));

    @Bean
    public AnnotationAwareAspectJAutoProxyCreator annotationAwareAspectJAutoProxyCreator() {
        return new AnnotationAwareAspectJAutoProxyCreator();
    }

    @Around("execution(* com.epam.theater.service.BookingService.bookTickets(..)) && args(tickets)")
    public Object checkLucky(ProceedingJoinPoint pjp, Set<Ticket> tickets) throws Throwable {
        tickets.forEach(t -> {
            if (luckyCalculate()) {
                t.getEvent().setBasePrice(0);
            }
        });
        tickets.forEach(i -> logger.info("Price " + i.getEvent().getBasePrice()));
        return pjp.proceed();
    }

    private boolean luckyCalculate() {
        if (random.nextInt() * 10 > 5) {
            logger.info("Lucky user");
            return true;
        } else {
            return false;
        }
    }
}
