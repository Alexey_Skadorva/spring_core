package com.epam.theater.aspects;

import com.epam.theater.dao.impl.CounterDaoImpl;
import com.epam.theater.entity.Event;
import com.epam.theater.entity.Ticket;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.aop.aspectj.annotation.AnnotationAwareAspectJAutoProxyCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;

import java.util.Set;
import java.util.logging.Logger;

@Aspect
public class CounterAspect {

    @Autowired
    private CounterDaoImpl counterDao;

    final static Logger logger = Logger.getLogger(String.valueOf(CounterAspect.class));

    @Bean
    public AnnotationAwareAspectJAutoProxyCreator annotationAwareAspectJAutoProxyCreator() {
        return new AnnotationAwareAspectJAutoProxyCreator();
    }

    @AfterReturning(pointcut = "execution(* com.epam.theater.service.EventService.getByName(..))", returning = "event")
    private void eventGetByName(Object event) {
        logger.info("Get event by name " + (Event) event);
        counterDao.eventCallingByName((Event) event);
    }

    @AfterReturning(pointcut = "execution(* com.epam.theater.service.BookingService.getTicketsPrice(..)) && args(event,..)")
    private void getPriceOfEvent(Event event) {
        logger.info("Get price of event " + (Event) event);
        counterDao.eventsPriceQueried((Event) event);
    }


    @After("execution(* com.epam.theater.service.BookingService.bookTickets(..)) && args(tickets)")
    private void bookTicket(Set<Ticket> tickets) {
        logger.info("Book " + tickets.size() + " tickets");
        tickets.forEach(t -> counterDao.eventsBookTicket(t.getEvent()));
    }
}
