package com.epam.theater.service.impl;

import com.epam.theater.entity.Auditorium;
import com.epam.theater.service.AuditoriumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class AuditoriumServiceImpl implements AuditoriumService {

    @Autowired
    private Set<Auditorium> auditoriums = new HashSet<>();

    @Override
    public Set<Auditorium> getAll() {
        return auditoriums;
    }

    @Override
    public Auditorium getByName(String name) {
        Optional<Auditorium> auditorium = auditoriums.stream().filter(n -> n.getName().equals(name)).findFirst();
        return auditorium.isPresent() ? auditorium.get() : null;
    }
}
