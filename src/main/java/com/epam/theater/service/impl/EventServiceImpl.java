package com.epam.theater.service.impl;

import com.epam.theater.dao.EventDao;
import com.epam.theater.entity.Event;
import com.epam.theater.service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class EventServiceImpl implements EventService {

    @Autowired
    private EventDao eventDao;

    @Override
    public Event getByName(String name) {
        return eventDao.getByName(name);
    }

    @Override
    public Event save(Event event) {
        return eventDao.save(event);
    }

    @Override
    public void remove(Event event) {
        eventDao.remove(event);
    }

    @Override
    public Event getById(Long id) {
        return eventDao.getById(id);
    }

    @Override
    public Collection<Event> getAll() {
        return eventDao.getAll();
    }
}
