package com.epam.theater.service.impl;

import com.epam.theater.discount.DiscountStrategy;
import com.epam.theater.entity.Event;
import com.epam.theater.entity.User;
import com.epam.theater.service.DiscountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class DiscountServiceImpl implements DiscountService {

    @Autowired
    private List<DiscountStrategy> discountStrategies = new ArrayList<>();

    public byte getDiscount(User user, Event event, LocalDateTime airDateTime, long numberOfTickets) {

        List<Byte> discounts = new ArrayList<>();
        for (DiscountStrategy discountStrategy : discountStrategies) {
            discounts.add(discountStrategy.getDiscount(user, event, airDateTime, numberOfTickets));
        }
        return Collections.max(discounts);
    }
}
