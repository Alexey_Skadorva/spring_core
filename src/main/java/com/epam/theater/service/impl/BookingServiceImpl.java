package com.epam.theater.service.impl;

import com.epam.theater.dao.BookingDao;
import com.epam.theater.entity.*;
import com.epam.theater.service.AuditoriumService;
import com.epam.theater.service.BookingService;
import com.epam.theater.service.DiscountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class BookingServiceImpl implements BookingService {

    @Autowired
    private AuditoriumService auditoriumService;

    @Autowired
    private DiscountService discountService;

    @Autowired
    private BookingDao bookingDao;

    @Autowired
    private Auditorium auditorium;

    @Override
    public double getTicketsPrice(Event event, LocalDateTime dateTime, User user, Set<Long> seats) {
        byte discount = discountService.getDiscount(user, event, dateTime, seats.size());
        long numberOfVipSeats = auditorium.countVipSeats(seats);
        double coef = event.getRating() == EventRating.MID ? 1 : event.getRating() == EventRating.HIGH ? 1.2 : 0.8;
        double price = ((100 - discount) / 100) * coef * ((seats.size() - numberOfVipSeats) * event.getBasePrice() + numberOfVipSeats
                * 2 * event.getBasePrice());
        return price;
    }

    @Override
    public void bookTickets(Set<Ticket> tickets) {
        for (Ticket ticket : tickets) {
            User user = ticket.getUser();
            user.addTicket(ticket);
        }
        bookingDao.bookTickets(tickets);
    }

    @Override
    public Set<Ticket> getPurchasedTicketsForEvent(Event event, LocalDateTime dateTime) {
        Set<Ticket> tickets = bookingDao.getAllTicketsToEvent(event, dateTime);
        return tickets.stream().filter(t -> t.getUser() != null).collect(Collectors.toSet());
    }
}
