package com.epam.theater.service;

import com.epam.theater.entity.Event;

public interface EventService extends AbstractDomainObjectService<Event> {

    /**
     * Finding event by name
     *
     * @param name Name of the event
     * @return found event or <code>null</code>
     */
    Event getByName(String name);
}
