package com.epam.theater.service;

import com.epam.theater.entity.User;

public interface UserService extends AbstractDomainObjectService<User> {

    /**
     * Finding user by email
     *
     * @param email Email of the user
     * @return found user or <code>null</code>
     */
    User getUserByEmail(String email);
}
