package com.epam.theater.mappers;

import com.epam.theater.entity.Event;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class EventRowMapper implements RowMapper {
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        Event event = new Event();
        event.setId(rs.getLong("ID"));
        event.setName(rs.getString("NAME"));
        event.setBasePrice(rs.getDouble("BASEPRICE"));
        return event;
    }
}
