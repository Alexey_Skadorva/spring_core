package com.epam.theater.mappers;

import com.epam.theater.dao.impl.EventDaoImpl;
import com.epam.theater.dao.impl.UserDaoImpl;
import com.epam.theater.entity.Ticket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TicketRowMapper implements RowMapper {
    @Autowired
    private EventDaoImpl eventDao;

    @Autowired
    private UserDaoImpl userDao;

    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        Ticket ticket = new Ticket();
        ticket.setId(rs.getLong("ID"));
        ticket.setEvent(eventDao.getById(rs.getLong("EVENT_ID")));
        ticket.setUser(userDao.getById(rs.getLong("USER_ID")));
        ticket.setSeat(rs.getLong("SEAT"));
        return ticket;
    }
}
