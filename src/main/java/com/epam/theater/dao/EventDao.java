package com.epam.theater.dao;

import com.epam.theater.entity.Event;

/**
 * Created by Alexey_Skadorva on 4/7/2016.
 */
public interface EventDao extends AbstractDomainObjectDao<Event> {
    /**
     * Finding event by name
     *
     * @param name Name of the event
     * @return found event or <code>null</code>
     */
    Event getByName(String name);
}
