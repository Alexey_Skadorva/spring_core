package com.epam.theater.dao;

import com.epam.theater.entity.User;

/**
 * Created by Alexey_Skadorva on 4/7/2016.
 */
public interface UserDao extends AbstractDomainObjectDao<User> {
    /**
     * Finding user by email
     *
     * @param email Email of the user
     * @return found user or <code>null</code>
     */
    User getUserByEmail(String email);
}
