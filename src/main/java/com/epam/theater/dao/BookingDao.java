package com.epam.theater.dao;

import com.epam.theater.entity.Event;
import com.epam.theater.entity.Ticket;

import java.time.LocalDateTime;
import java.util.Set;

/**
 * Created by Alexey_Skadorva on 4/7/2016.
 */
public interface BookingDao {

    /**
     * Books tickets in internal system. If user is not
     * <code>null</code> in a ticket then booked tickets are saved with it
     *
     * @param tickets Set of tickets
     */
    void bookTickets(Set<Ticket> tickets);

    /**
     * Getting all purchased tickets for event on specific air date and time
     *
     * @param event    Event to get tickets for
     * @param dateTime Date and time of airing of event
     * @return set of all purchased tickets
     */
    Set<Ticket> getAllTicketsToEvent(Event event, LocalDateTime dateTime);
}
