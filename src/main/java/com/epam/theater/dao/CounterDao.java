package com.epam.theater.dao;

import com.epam.theater.entity.Event;

/**
 * Created by Alexey_Skadorva on 4/25/2016.
 */
public interface CounterDao {
    void eventCallingByName(Event event);

    void eventsBookTicket(Event event);

    void eventsPriceQueried(Event event);

}
