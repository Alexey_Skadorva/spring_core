package com.epam.theater.dao.impl;

import com.epam.theater.dao.CounterDao;
import com.epam.theater.entity.Event;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class CounterDaoImpl implements CounterDao {

    private final static String ADD_COUNTER = "INSERT INTO COUNTER(EVENT_ID, STATUS) VALUES(?,?)";

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    public void eventCallingByName(Event event) {
        jdbcTemplate.update(ADD_COUNTER, event.getId(), "CALLING");
    }

    @Override
    public void eventsBookTicket(Event event) {
        jdbcTemplate.update(ADD_COUNTER, event.getId(), "BOOK");
    }

    @Override
    public void eventsPriceQueried(Event event) {
        jdbcTemplate.update(ADD_COUNTER, event.getId(), "PRICE");
    }
}
