package com.epam.theater.dao.impl;

import com.epam.theater.dao.UserDao;
import com.epam.theater.entity.User;
import com.epam.theater.mappers.UserRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.logging.Logger;

@Repository
public class UserDaoImpl implements UserDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    final static Logger logger = Logger.getLogger(String.valueOf(UserDaoImpl.class));

    private final static String ADD_USER = "INSERT INTO USERS(ID, FIRSTNAME, LASTNAME, EMAIL) VALUES(?,?,?,?)";
    private final static String DELETE_USER = "DELETE FROM USERS WHERE ID = ?";
    private final static String GET_USER_BY_ID = "SELECT * FROM USERS WHERE ID = ?";
    private final static String GET_USER_BY_EMAIL = "SELECT * FROM USERS WHERE EMAIL = ?";
    private final static String GET_ALL_USERS = "SELECT * FROM USERS";

    @Override
    public User save(User user) {
        logger.info("Save user " + user);
        jdbcTemplate.update(ADD_USER, user.getId(), user.getFirstName(), user.getLastName(), user.getEmail());
        return user;
    }

    @Override
    public void remove(User user) {
        logger.info("Remove user ");
        jdbcTemplate.update(DELETE_USER, user.getId());
    }

    @Override
    public User getById(Long id) {
        logger.info("Search user with id = " + id);
        return (User) jdbcTemplate.query(GET_USER_BY_ID, new Object[]{id}, new UserRowMapper()).get(0);
    }

    @Override
    public Collection<User> getAll() {
        logger.info("Get all users");
        return jdbcTemplate.query(GET_ALL_USERS, new BeanPropertyRowMapper(User.class));
    }

    @Override
    public User getUserByEmail(String email) {
        logger.info("Search user with email " + email);
        return (User) jdbcTemplate.query(GET_USER_BY_EMAIL, new Object[]{email}, new UserRowMapper()).get(0);
    }
}
