package com.epam.theater.dao.impl;

import com.epam.theater.dao.EventDao;
import com.epam.theater.entity.Event;
import com.epam.theater.mappers.EventRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.logging.Logger;

@Repository
public class EventDaoImpl implements EventDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    final static Logger logger = Logger.getLogger(String.valueOf(EventDaoImpl.class));

    private final static String ADD_EVENT = "INSERT INTO EVENTS(ID, NAME, BASEPRICE, RATING) VALUES(?,?,?,?)";
    private final static String DELETE_EVENT = "DELETE FROM EVENTS WHERE ID = ?";
    private final static String GET_EVENT_BY_ID = "SELECT * FROM EVENTS WHERE ID = ?";
    private final static String GET_EVENT_BY_NAME = "SELECT * FROM EVENTS WHERE NAME = ?";
    private final static String GET_ALL_EVENTS = "SELECT * FROM EVENTS";

    @Override
    public Event save(Event event) {
        logger.info("Save event " + event);
        jdbcTemplate.update(ADD_EVENT, event.getId(), event.getName(), event.getBasePrice(), event.getRating().toString());
        return event;
    }

    @Override
    public Event getByName(String name) {
        logger.info("Search event with name " + name);
        return (Event) jdbcTemplate.query(GET_EVENT_BY_NAME, new Object[]{name}, new EventRowMapper()).get(0);
    }

    @Override
    public void remove(Event event) {
        logger.info("Remove event");
        jdbcTemplate.update(DELETE_EVENT, event.getId());
    }

    @Override
    public Event getById(Long id) {
        logger.info("Search event with id = " + id);
        return (Event) jdbcTemplate.query(GET_EVENT_BY_ID, new Object[]{id}, new EventRowMapper()).get(0);
    }

    @Override
    public Collection getAll() {
        logger.info("Get all events");
        return jdbcTemplate.query(GET_ALL_EVENTS, new BeanPropertyRowMapper(Event.class));
    }
}
