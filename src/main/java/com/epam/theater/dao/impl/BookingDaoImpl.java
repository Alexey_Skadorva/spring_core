package com.epam.theater.dao.impl;

import com.epam.theater.dao.BookingDao;
import com.epam.theater.entity.Event;
import com.epam.theater.entity.Ticket;
import com.epam.theater.mappers.TicketRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Set;
import java.util.logging.Logger;

@Repository
public class BookingDaoImpl implements BookingDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private final static String BOOK_TICKET = "INSERT INTO TICKETS(ID, SEAT, USER_ID, EVENT_ID) VALUES(?,?,?,?)";
    private final static String GET_ALL_TICKETS_TO_EVENT = "SELECT * FROM TICKETS WHERE EVENT_ID = ?";

    @Override
    public void bookTickets(Set<Ticket> tickets) {
        tickets.forEach(t -> {
            if (t.getUser() != null)
                jdbcTemplate.update(BOOK_TICKET, t.getId(), t.getSeat(), t.getUser().getId(), t.getEvent().getId());
        });
    }

    @Override
    public Set<Ticket> getAllTicketsToEvent(Event event, LocalDateTime dateTime) {
        return (Set<Ticket>) jdbcTemplate.queryForObject(GET_ALL_TICKETS_TO_EVENT, new Object[]{event.getId()}, new TicketRowMapper());
    }
}
